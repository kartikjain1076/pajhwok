import React, { Component } from "react";
import App from "./App";
import { Provider } from "react-redux";
import Store from "./Store/Store";
import SplashScreen from "react-native-splash-screen";

export default class Redux extends Component {
  render() {
    return (
      <Provider store={Store}>
        <App />
      </Provider>
    );
  }
}
