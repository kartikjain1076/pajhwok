import { DrawerNavigator } from "react-navigation";
import DrawerContent from "./component/DrawerContent/index";
import React, { Component } from "react";
import secondDrawer from "./secondDrawer";

const CustomDrawerContentComponent = props => <DrawerContent />;

export default DrawerNavigator(
  {
    Homee: {
      screen: secondDrawer
    }
  },
  {
    initialRouteName: "Homee",
    drawerWidth: 300,
    drawerPosition: "left",
    contentComponent: CustomDrawerContentComponent
  }
);
