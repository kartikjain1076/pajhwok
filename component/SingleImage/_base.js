import React from "react";
import { View, Text, Image } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { baseURL } from "../../config";
import { styles } from "./_style";
import { ScrollView } from "react-native-gesture-handler";

export default class SingleImageBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      nid: ""
    };
  }

  componentDidMount = async () => {
    await this.setState({ nid: this.props.navigation.getParam("nid") });
    await this.fetchNews();
  };

  // fetch the data according to the language
  fetchNews = async () => {
    let response = await axios.get(
      `${baseURL}${this.props.language}/json/photo_content?id=${this.state.nid}`
    );
    await this.setState({ data: response.data });
  };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.state.data.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <ScrollView>
          <View style={styles.main}>
            <Text
              style={[
                styles.title,
                { fontSize: this.props.allFont.fontSizeTitle }
              ]}
            >
              {this.state.data[0].title}
            </Text>
            <Image
              source={{ uri: this.state.data[0].image }}
              style={styles.body}
            />
            <HTML
              html={this.state.data[0].body}
              containerStyle={styles.bodyText}
            />
          </View>
        </ScrollView>
      );
    }
  };
}
