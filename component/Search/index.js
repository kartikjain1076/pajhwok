import SearchBase from "./_base";
import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

class Search extends SearchBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    lan: state.reducer.lan,
    allFont: state.reducer.allFont
  };
};

export default withNavigation(connect(mapStateToProps)(Search));
