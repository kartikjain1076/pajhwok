import React, { Component } from "react";
import { View, ScrollView, Dimensions, TextInput } from "react-native";
import Header from "../header";
import { styles } from "./_style";
import SearchBar from "../SearchBar";
export default function _render() {
  return (
    <ScrollView
      stickyHeaderIndices={[0]}
      keyboardShouldPersistTaps="always"
      onScroll={this.handleScroll}
    >
      <Header />
      <SearchBar />
      <View>
        {this.showHead()}
        {this.showProgress()}
      </View>
      <View style={styles.progress}>{this.hideProgress()}</View>
    </ScrollView>
  );
}
