import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableHighlight,
  ScrollView,
  TextInput
} from "react-native";
import axios from "axios";
import * as Progress from "react-native-progress";
import { baseURL } from "../../config";
import { styles } from "./_style";

export default class Search extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      offset: 0,
      Content: "",
      blank: false,
      head: false,
      showContent: "",
      end: false
    };
  }

  componentDidMount = async () => {
    await this.setState({
      Content: this.props.navigation.state.params.Content
    });
    this.fetchNews();
  };

  // fetch the data according to the language
  fetchNews = async () => {
    this.setState({
      head: true,
      blank: false,
      showContent: this.state.Content
    });
    let response = await axios.get(
      `${baseURL}${this.props.language}/json/search_content?title=${
        this.state.Content
      }&offset=${this.state.offset}&limit=10`
    );
    let temp = this.state.news;
    response.data.map(data => {
      temp.push(data);
    });
    await this.setState({ news: temp });
    if (this.state.news.length == 0) {
      this.setState({ blank: true });
    }
    await this.setState({ head: false });
    if (response.data.length < 10) this.setState({ end: true });
  };
  // update the state and call the required functions on the navigation to the same screen
  shouldComponentUpdate = async (nextProps, nextState) => {
    if (nextProps.navigation.state.params.Content != this.state.Content) {
      await this.setState({
        Content: nextProps.navigation.state.params.Content,
        news: [],
        offset: 0,
        blank: false,
        showContent: "",
        end: false
      });
      this.fetchNews();
    }
  };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.state.blank) {
      return (
        <View style={styles.header2}>
          <Text
            style={[
              styles.text2,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {
              this.props.lan.translation[
                "Data you are searching for is not available. Please change your search"
              ][this.props.language]
            }
          </Text>
        </View>
      );
    } else {
      if (this.state.news.length == 0 && this.state.head) {
        return (
          <View style={styles.progress}>
            <Progress.Circle size={30} indeterminate={true} color="#253C80" />
          </View>
        );
      } else {
        return this.state.news.map(news => {
          return (
            <TouchableHighlight
              onPress={() =>
                this.props.navigation.navigate("SingleNews", { nid: news.nid })
              }
            >
              <View style={styles.header2}>
                <Text
                  style={[
                    styles.text,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {news.title}
                </Text>
                <View style={styles.image}>
                  <Image
                    source={{ uri: news.image }}
                    style={styles.imageContain}
                  />
                </View>
              </View>
            </TouchableHighlight>
          );
        });
      }
    }
  };

  // detect the scroll position and on the end of scroll change the offset state and fetch the data according to it
  handleScroll = async event => {
    if (
      event.nativeEvent.contentOffset.y >=
      event.nativeEvent.contentSize.height -
        event.nativeEvent.layoutMeasurement.height -
        3
    ) {
      await this.setState({ offset: this.state.offset + 10 });
      this.fetchNews();
    }
  };
  // show the progress spinner until the data is coming from the backend
  hideProgress = () => {
    if (this.state.end) {
    } else if (this.state.news.length > 0)
      return <Progress.Circle size={30} indeterminate={true} color="#253C80" />;
  };
  // display the seach title
  showHead = () => {
    return (
      <View style={styles.header}>
        <Text
          style={[
            styles.topic,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          Search Result for : {this.state.Content}
        </Text>
      </View>
    );
  };
}
