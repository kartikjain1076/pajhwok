import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-between",
    backgroundColor: "#253C80",
    alignItems: "center",
    marginBottom: 2
  },
  topic: {
    marginLeft: "2%",
    color: "#ffffff",
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 1,
    paddingBottom: (height / 100) * 1
  },
  viewMore: {
    marginRight: "2%",
    color: "#000000",
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 1,
    paddingBottom: (height / 100) * 1,
    alignSelf: "flex-end"
  },
  header2: {
    display: "flex",
    backgroundColor: "white",
    width: "100%",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: { width: 20, height: 20 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 3,
    marginBottom: 2
  },
  text: {
    width: "70%",
    flexWrap: "wrap",
    alignSelf: "center",
    marginLeft: (width / 100) * 2,
    fontFamily: "AverageSans-Regular",
    color: "#000000"
  },
  text2: {
    width: "100%",
    flexWrap: "wrap",
    alignSelf: "center",
    marginLeft: (width / 100) * 2,
    fontFamily: "AverageSans-Regular",
    color: "#000000"
  },
  image: {
    width: "25%",
    marginLeft: "5%",
    marginRight: "2%",
    alignContent: "center",
    marginTop: (height / 100) * 2,
    marginBottom: (height / 100) * 2
  },
  imageContain: {
    height: (height / 100) * 10,
    width: (width / 100) * 20,
    marginRight: (width / 100) * 5
  },
  progress: {
    marginLeft: (width / 100) * 45,
    marginTop: (height / 100) * 1
  },
  wrapper: {
    width: "100%",
    flex: 1,
    alignContent: "flex-start"
  },
  headerStyle: {
    backgroundColor: "#000000"
  },
  logoStyle: {
    height: (height / 100) * 5,
    width: (width / 100) * 10
  }
});
