import React, { Component } from "react";
import SplashScreen from "react-native-splash-screen";
import axios from "axios";
import { baseURL } from "../../config";
export default class Drawer2ContentBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      start: false
    };
  }

  // called when the language is changed to english and it update the redux states.
  changeToEnglish = async () => {
    let data = { name: "language", value: "en" };
    await this.props.change(data);
    data = { name: "recent", value: [] };
    await this.props.change(data);
    data = { name: "feature", value: [] };
    await this.props.change(data);
    data = { name: "interview", value: [] };
    await this.props.change(data);
    data = { name: "opinion", value: [] };
    await this.props.change(data);
    data = { name: "photo", value: [] };
    await this.props.change(data);
    data = { name: "video", value: [] };
    await this.props.change(data);
    data = { name: "slideshow", value: [] };
    await this.props.change(data);
    await this.props.navigation.navigate("Homeee");
    this.recent("en");
    this.feature("en");
    this.interview("en");
    this.opinion("en");
    this.photo("en");
    this.video("en");
    this.slideshow("en");
    //  data = {name : 'lan', value : English}
    //   this.props.change(data);
  };

  // called when the language is changed to dari and it update the redux states.
  changeToDari = async () => {
    let data = { name: "language", value: "dr" };
    await this.props.change(data);
    data = { name: "recent", value: [] };
    await this.props.change(data);
    data = { name: "feature", value: [] };
    await this.props.change(data);
    data = { name: "interview", value: [] };
    await this.props.change(data);
    data = { name: "opinion", value: [] };
    await this.props.change(data);
    data = { name: "photo", value: [] };
    await this.props.change(data);
    data = { name: "video", value: [] };
    await this.props.change(data);
    data = { name: "slideshow", value: [] };
    await this.props.change(data);
    await this.props.navigation.navigate("Homeee");
    this.recent("dr");
    this.feature("dr");
    this.interview("dr");
    this.opinion("dr");
    this.photo("dr");
    this.video("dr");
    this.slideshow("dr");
    //  data = {name : 'lan', value : Dari}
    //   this.props.change(data);
  };

  // called when the language is changed to pashto and it update the redux states.
  changeToPashto = async () => {
    let data = { name: "language", value: "ps" };
    await this.props.change(data);
    data = { name: "recent", value: [] };
    await this.props.change(data);
    data = { name: "feature", value: [] };
    await this.props.change(data);
    data = { name: "interview", value: [] };
    await this.props.change(data);
    data = { name: "opinion", value: [] };
    await this.props.change(data);
    data = { name: "photo", value: [] };
    await this.props.change(data);
    data = { name: "video", value: [] };
    await this.props.change(data);
    data = { name: "slideshow", value: [] };
    await this.props.change(data);
    await this.props.navigation.navigate("Homeee");
    this.recent("ps");
    this.feature("ps");
    this.interview("ps");
    this.opinion("ps");
    this.photo("ps");
    this.video("ps");
    this.slideshow("ps");
    //  data = {name : 'lan', value : Pashto}
    //   this.props.change(data);
  };

  // get the recent article data according to language selected and set the redux state.
  recent = async value => {
    const response = await axios.get(
      `${baseURL}${value}/json/recent_article?offset=2&limit=5`
    );
    const data = {
      name: "recent",
      value: [
        {
          title: response.data[0].title,
          image: response.data[0].image,
          nid: response.data[0].nid
        },
        {
          title: response.data[1].title,
          image: response.data[1].image,
          nid: response.data[1].nid
        },
        {
          title: response.data[2].title,
          image: response.data[2].image,
          nid: response.data[2].nid
        },
        {
          title: response.data[3].title,
          image: response.data[3].image,
          nid: response.data[3].nid
        },
        {
          title: response.data[4].title,
          image: response.data[4].image,
          nid: response.data[4].nid
        }
      ]
    };
    await this.props.change(data);
  };
  // get the feature article data according to language selected and set the redux state.
  feature = async value => {
    const response = await axios.get(
      `${baseURL}${value}/json/category_content?tid=27&front=1`
    );
    const data = {
      name: "feature",
      value: [
        {
          title: response.data[0].title,
          image: response.data[0].image,
          nid: response.data[0].nid
        },
        {
          title: response.data[1].title,
          image: response.data[1].image,
          nid: response.data[1].nid
        },
        {
          title: response.data[2].title,
          image: response.data[2].image,
          nid: response.data[2].nid
        },
        {
          title: response.data[3].title,
          image: response.data[3].image,
          nid: response.data[3].nid
        },
        {
          title: response.data[4].title,
          image: response.data[4].image,
          nid: response.data[4].nid
        }
      ]
    };
    await this.props.change(data);
  };
  // get the interview data according to language selected and set the redux state.
  interview = async value => {
    const response = await axios.get(
      `${baseURL}${value}/json/category_content?tid=28&front=1`
    );
    const data = {
      name: "interview",
      value: [
        {
          title: response.data[0].title,
          image: response.data[0].image,
          nid: response.data[0].nid
        },
        {
          title: response.data[1].title,
          image: response.data[1].image,
          nid: response.data[1].nid
        },
        {
          title: response.data[2].title,
          image: response.data[2].image,
          nid: response.data[2].nid
        },
        {
          title: response.data[3].title,
          image: response.data[3].image,
          nid: response.data[3].nid
        },
        {
          title: response.data[4].title,
          image: response.data[4].image,
          nid: response.data[4].nid
        }
      ]
    };
    await this.props.change(data);
  };
  // get the opinion data according to language selected and set the redux state.
  opinion = async value => {
    const response = await axios.get(
      `${baseURL}${value}/json/recent_opinion?offset=2&limit=5`
    );
    const data = {
      name: "opinion",
      value: [
        { title: response.data[0].title },
        { title: response.data[1].title },
        { title: response.data[2].title }
      ]
    };
    await this.props.change(data);
  };
  // get the recent photo data according to language selected and set the redux state.
  photo = async value => {
    const response = await axios.get(`${baseURL}${value}/json/photo_content`);
    const data = {
      name: "photo",
      value: [
        {
          title: response.data[0].title,
          url: response.data[0].image,
          nid: response.data[0].nid
        },
        {
          title: response.data[1].title,
          url: response.data[1].image,
          nid: response.data[1].nid
        },
        {
          title: response.data[2].title,
          url: response.data[2].image,
          nid: response.data[2].nid
        },
        {
          title: response.data[3].title,
          url: response.data[3].image,
          nid: response.data[3].nid
        }
      ]
    };
    await this.props.change(data);
  };
  // get the recent video data according to language selected and set the redux state.
  video = async value => {
    const response = await axios.get(
      `${baseURL}${value}/json/new_video_content?front=1`
    );
    const data = {
      name: "video",
      value: [
        {
          title: response.data[0].title,
          url: response.data[0].images_url.large,
          nid: response.data[0].nid
        },
        {
          title: response.data[1].title,
          url: response.data[1].images_url.large,
          nid: response.data[1].nid
        },
        {
          title: response.data[2].title,
          url: response.data[2].images_url.large,
          nid: response.data[2].nid
        },
        {
          title: response.data[3].title,
          url: response.data[3].images_url.large,
          nid: response.data[3].nid
        }
      ]
    };
    await this.props.change(data);
  };
  // get the images data that are in the slideshow according to language selected and set the redux state.
  slideshow = async value => {
    response = await axios.get(`${baseURL}${value}/json/home_slider`);
    const data = {
      name: "slideshow",
      value: [
        { title: response.data[0].title, url: response.data[0].image },
        { title: response.data[1].title, url: response.data[1].image },
        { title: response.data[2].title, url: response.data[2].image },
        { title: response.data[3].title, url: response.data[3].image },
        { title: response.data[4].title, url: response.data[4].image }
      ]
    };
    await this.props.change(data);
  };
}
