import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  main: {
    backgroundColor: "#3f3f3f",
    height: height
  },
  title: {
    fontFamily: "AverageSans-Regular",
    marginLeft: (width / 100) * 5,
    alignSelf: "flex-start",
    marginTop: (height / 100) * 0.5,
    color: "#5e5d5d"
  },
  titleContainer: {
    width: width,
    height: (height / 100) * 4,
    backgroundColor: "#282828",
    marginTop: (height / 100) * 4,
    borderTopColor: "#5e5d5d",
    borderTopWidth: (height / 100) * 0.2,
    borderBottomColor: "#5e5d5d",
    borderBottomWidth: (height / 100) * 0.2
  },
  listItem: {
    marginLeft: (width / 100) * 2,
    marginTop: (height / 100) * 1,
    flexDirection: "row"
  },
  listText: {
    color: "#ffffff",
    fontFamily: "AverageSans-Regular",
    marginLeft: (width / 100) * 2
  },
  languageListText: {
    color: "#ffffff",
    fontFamily: "AverageSans-Regular",
    marginLeft: (width / 100) * 5,
    marginTop: (height / 100) * 0.3
  }
});
