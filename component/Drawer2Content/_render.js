import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableHighlight,
  Keyboard
} from "react-native";
import { styles } from "./_style";
import Icon from "react-native-vector-icons/dist/Octicons";
export default function _render() {
  Keyboard.dismiss();
  return (
    <View style={styles.main}>
      <View style={styles.titleContainer}>
        <Text
          style={[
            styles.title,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {
            this.props.lan.translation["Application Settings"][
              this.props.language
            ]
          }
        </Text>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight onPress={() => this.props.drawer.navigate("Login")}>
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["Login"][this.props.language]}
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <Text
          style={[
            styles.listText,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {this.props.lan.translation["Switch Language"][this.props.language]}
        </Text>
      </View>
      <View style={styles.listItem}>
        <TouchableHighlight onPress={() => this.changeToEnglish()}>
          <Text
            style={[
              styles.languageListText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            English
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <TouchableHighlight onPress={() => this.changeToDari()}>
          <Text
            style={[
              styles.languageListText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            داری
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <TouchableHighlight onPress={() => this.changeToPashto()}>
          <Text
            style={[
              styles.languageListText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            پښتو
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("FontSize")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["Font Size"][this.props.language]}
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.titleContainer}>
        <Text
          style={[
            styles.title,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {this.props.lan.translation["Information"][this.props.language]}
        </Text>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("Homeee")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["Unread News"][this.props.language]}
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("AboutUs")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["About Us"][this.props.language]}
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("TermsAndConditions")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {
              this.props.lan.translation["Terms \u0026 Conditions"][
                this.props.language
              ]
            }
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("ContactUs")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["Contact Us"][this.props.language]}
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("Homeee")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {
              this.props.lan.translation["Personal Subscription"][
                this.props.language
              ]
            }
          </Text>
        </TouchableHighlight>
      </View>
      <View style={styles.listItem}>
        <Icon name="primitive-dot" size={20} color="#5e5d5d" />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate("Feedback")}
        >
          <Text
            style={[
              styles.listText,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["Feedback"][this.props.language]}
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}
