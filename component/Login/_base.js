import React, { Component } from "react";
export default class LoginBase extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      text: "",
      username: "",
      password: "",
      usernameError: "",
      passwordError: ""
    };
  }

  // validate the data that is entered in the text input
  validateRegister = text => {
    let textUsername = this.state.username;
    let textPassword = this.state.password;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let usernameResult = reg.test(textUsername);
    if (textUsername === "" && textPassword === "") {
      this.setState({
        usernameError: this.props.lan.translation["Email Field is Empty"][
          this.props.language
        ],
        passwordError: this.props.lan.translation["Password Field is Empty"][
          this.props.language
        ]
      });
      return false;
    } else if (
      textUsername === "" &&
      this.state.password.length &&
      this.state.password.length > 6
    ) {
      this.setState({
        usernameError: this.props.lan.translation["Email Field is Empty"][
          this.props.language
        ],
        passwordError: ""
      });
      return false;
    } else if (
      textUsername === "" &&
      this.state.password.length > 0 &&
      this.state.password.length < 6
    ) {
      this.setState({
        usernameError: this.props.lan.translation["Email Field is Empty"][
          this.props.language
        ],
        passwordError: this.props.lan.translation["Password is Too Short"][
          this.props.language
        ]
      });
      return false;
    } else if (textPassword === "" && usernameResult == false) {
      this.setState({
        passwordError: this.props.lan.translation["Password Field is Empty"][
          this.props.language
        ],
        usernameError: this.props.lan.translation["Email Pattern is Wrong"][
          this.props.language
        ]
      });

      return false;
    } else if (usernameResult === true && this.state.password === "") {
      this.setState({
        usernameError: "",
        passwordError: this.props.lan.translation["Password Field is Empty"][
          this.props.language
        ]
      });
      return false;
    } else if (usernameResult == false && this.state.password.length > 6) {
      this.setState({
        usernameError: this.props.lan.translation["Email Pattern is Wrong"][
          this.props.language
        ],
        passwordError: ""
      });
    } else if (
      usernameResult === true &&
      this.state.password.length > 0 &&
      this.state.password.length < 6
    ) {
      this.setState({
        passwordError: this.props.lan.translation["Password is Too Short"][
          this.props.language
        ],
        usernameError: ""
      });
      return false;
    } else if (
      this.state.password.length > 0 &&
      this.state.password.length < 6 &&
      usernameResult === false
    ) {
      this.setState({
        usernameError: this.props.lan.translation["Email Pattern is Wrong"][
          this.props.language
        ],
        passwordError: this.props.lan.translation["Password is Too Short"][
          this.props.language
        ]
      });
    } else if ((textUsername = "" && this.state.password.length > 6)) {
      this.setState({
        passwordError: "",
        usernameError: this.props.lan.translation["Email Field is Empty"][
          this.props.language
        ]
      });
      return false;
    } else {
      this.setState({ usernameError: "", passwordError: "" });
      return true;
    }
  };

  checkValidation = () => {
    const a = this.validateRegister();
    if (a) {
    } else {
    }
  };
}
