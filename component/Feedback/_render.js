import React, { Component } from "react";
import { View, ScrollView, Text, TextInput, Button } from "react-native";
import Header from "../header/index";
import { styles } from "./_style";
export default function _render() {
  return (
    <ScrollView>
      <View style={styles.wrapper}>
        <View style={styles.textWrapper}>
          <Text
            style={[
              styles.textStyle,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {" "}
            Send Us Your Feedback
          </Text>
        </View>
        <View style={styles.textInputWrapper}>
          <TextInput
            style={styles.textInputStyle}
            onChangeText={text => this.setState({ text })}
            value={this.state.text}
            placeholder="Enter Name"
          />
        </View>
        <View style={styles.textInputWrapper}>
          <TextInput
            style={styles.textInputStyle}
            onChangeText={text1 => this.setState({ text1 })}
            value={this.state.text1}
            placeholder="Enter Email"
          />
        </View>
        <View style={styles.textInputWrapper}>
          <TextInput
            style={styles.textInputStyle}
            onChangeText={text2 => this.setState({ text2 })}
            value={this.state.text2}
            placeholder="Enter Your Feedback"
          />
        </View>
        <View style={styles.buttonWrapper}>
          <Button title="Submit" color="#001a4d" style={styles.buttonStyle} />
        </View>
      </View>
    </ScrollView>
  );
}
