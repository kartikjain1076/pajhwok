import React, { Component } from "react";
import { View, ScrollView, Image, Keyboard } from "react-native";
import { Appbar } from "react-native-paper";
import { styles } from "./_style";
export default function _render() {
  return (
    <ScrollView keyboardShouldPersistTaps="always">
      <View style={styles.wrapper}>
        <Appbar.Header style={styles.headerStyle}>
          <Appbar.Action icon="menu" onPress={this.props.drawer.openDrawer} />
          <Image
            source={require("../../public/1.png")}
            style={styles.logoStyle}
          />
          <Appbar.Content
            title={this.props.lan.translation["PAJHWOK"][this.props.language]}
            subtitle={
              this.props.lan.translation["AFGHAN NEWS"][this.props.language]
            }
            titleStyle={[
              styles.title,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
            subtitleStyle={[
              styles.subTitle,
              { fontSize: this.props.allFont.fontSizeSmall }
            ]}
          />
          <Appbar.Action
            icon="settings"
            style={{ justifyContent: "flex-end" }}
            onPress={() => this.props.navigation.openDrawer()}
          />
        </Appbar.Header>
      </View>
    </ScrollView>
  );
}
