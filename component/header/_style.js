import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  wrapper: {
    width: "100%",
    flex: 1,
    alignContent: "flex-start"
  },
  headerStyle: {
    backgroundColor: "#000000"
  },
  logoStyle: {
    height: (height / 100) * 5,
    width: (width / 100) * 10
  },
  title: {
    fontFamily: "AverageSans-Regular"
  },
  subTitle: {
    fontFamily: "AverageSans-Regular"
  }
});
