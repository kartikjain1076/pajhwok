import React, { Component } from "react";
import { View, Text, TouchableHighlight, Image } from "react-native";
import axios from "axios";
import { baseURL } from "../../config";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
import { intialState } from "../../Store/Reducer";
export default class PhotosBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      news: []
    };
  }

  // componentDidMount = async () => {
  //   await this.fetchNews();
  //   await this.changeRecent();
  // };

  // changeRecent = () => {
  //   const data = { name: "photo", value: this.state.news };
  //   this.props.change(data);
  // };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.props.photo.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return this.props.photo.map(news => {
        return (
          <TouchableHighlight
            onPress={() =>
              this.props.navigation.navigate("SingleImage", { nid: news.nid })
            }
          >
            <View style={styles.content}>
              <View style={styles.imageContainer}>
                <Image source={{ uri: news.image }} style={styles.image1} />
              </View>
              <Text
                style={[
                  styles.text,
                  { fontSize: intialState.allFont.fontSizeNormal }
                ]}
              >
                {news.title}
              </Text>
            </View>
          </TouchableHighlight>
        );
      });
    }
  };

  // fetchNews = async () => {
  //   let response = await axios.get(
  //     `${baseURL}${this.props.language}/json/photo_content?front=1&limit=4`
  //   );
  //   await this.setState({
  //     news:response.data
  //   });
  // };
}
