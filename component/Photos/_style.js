import { StyleSheet, Dimensions } from "react-native";
import { intialState } from "../../Store/Reducer";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-between",
    backgroundColor: "#253C80",
    alignItems: "center",
    marginBottom: 2
  },
  topic: {
    marginLeft: "2%",
    color: "white",
    fontSize: intialState.allFont.fontSizeNormal,
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 0.5,
    paddingBottom: (height / 100) * 0.5
  },
  viewMore: {
    marginRight: "2%",
    color: "#000000",
    fontSize: intialState.allFont.fontSizeNormal,
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 0.5,
    paddingBottom: (height / 100) * 0.5,
    alignSelf: "flex-end"
  },
  header2: {
    display: "flex",
    backgroundColor: "white",
    marginTop: (height / 100) * 2,
    marginBottom: (height / 100) * 2,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap"
  },
  imageContainer: {
    height: (height / 100) * 15,
    width: width > 420 ? (width / 100) * 20 : (width / 100) * 40
  },
  image1: {
    height: (height / 100) * 15,
    width: width > 420 ? (width / 100) * 20 : (width / 100) * 40
  },
  content: {
    flexDirection: "column",
    width: width > 420 ? (width / 100) * 20 : (width / 100) * 40,
    marginRight: (width / 100) * 1,
    marginLeft: (width / 100) * 1
  },
  text: {
    // flexWrap : 'wrap',
    fontFamily: "AverageSans-Regular",
    color: "#000000"
  }
});
