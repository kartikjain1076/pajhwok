import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import Header from "../header/index";
import { styles } from "./_style";

export default function _render() {
  return (
    <ScrollView
      stickyHeaderIndices={[0]}
      keyboardShouldPersistTaps="always"
      style={{ flex: 1 }}
    >
      <Header />
      <View style={styles.background}>{this.showProgress()}</View>
    </ScrollView>
  );
}
