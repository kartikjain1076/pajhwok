import React, { Component } from "react";
import { View, Text } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
import { baseURL } from "../../config";

export default class AboutUsBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      url: `${baseURL}json/page?nid=246473`
    };
  }
  //  In componentDidMount we check the selected language and change the url according to it
  componentDidMount = async () => {
    if (this.props.language == "ps")
      await this.setState({ url: `${baseURL}json/page?nid=246462` });
    else if (this.props.language == "dr")
      await this.setState({ url: `${baseURL}json/page?nid=246461` });
    this.fetchData();
  };

  // In this we get the data and set the state
  fetchData = async () => {
    let response = await axios.get(this.state.url);
    await this.setState({
      data: [{ title: response.data[0].title, body: response.data[0].body }]
    });
  };
  // while the data is being get a progress spinner will be shown
  showProgress = () => {
    if (this.state.data.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <View style={styles.main}>
          <Text
            style={[
              styles.title,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.state.data[0].title}
          </Text>
          <HTML html={this.state.data[0].body} containerStyle={styles.body} />
        </View>
      );
    }
  };
}
