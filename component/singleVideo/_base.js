import React, { Component } from "react";
import { View, Text, Image, Dimensions } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { baseURL } from "../../config";
import { styles } from "./_style";
import { ScrollView } from "react-native-gesture-handler";

const { height, width } = Dimensions.get("window");

export default class SingleVideoBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      id: 0,
      loader: true
    };
  }

  componentDidMount = async () => {
    await this.setState({ id: this.props.navigation.getParam("nid") });
    this.fetchData();
  };

  // fetch the data according to the language nad adjust the width of the iframe in the html recieved
  fetchData = async () => {
    let response = await axios.get(
      `${baseURL}${
        this.props.language
      }/json/article_contents_android?screen=detail&id=${
        this.state.id
      }&mobile=android&version_id=30.1.6`
    );
    let t = response.data;
    let temp = response.data[0].body;
    let length = temp.length;
    let flag = 0;
    for (let i = 0; i < length - 11; i++) {
      if (
        temp[i] == "w" &&
        temp[i + 1] == "i" &&
        temp[i + 2] == "d" &&
        temp[i + 3] == "t" &&
        temp[i + 4] == "h"
      ) {
        let a = temp.slice(0, i + 6);
        a = a + `${(width / 100) * 90}`;
        let b = a + temp.slice(i + 11, length);
        temp = b;
        t[0].body = temp;
        await this.setState({ data: t });
        flag = 1;
      }
    }
    if (flag == 0) await this.setState({ data: response.data });
    await this.setState({ loader: false });
  };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.state.data.length == 0 || this.state.loader) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <ScrollView>
          <View style={styles.main}>
            <Text
              style={[
                styles.title,
                { fontSize: this.props.allFont.fontSizeTitle }
              ]}
            >
              {this.state.data[0].title}
            </Text>
            <HTML html={this.state.data[0].body} containerStyle={styles.body} />
          </View>
        </ScrollView>
      );
    }
  };
}
