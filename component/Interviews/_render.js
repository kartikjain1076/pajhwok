import React, { Component } from "react";
import { View, TouchableHighlight, Text } from "react-native";
import Header from "../header";
import Icon from "react-native-vector-icons/dist/FontAwesome";

import { styles } from "./_style";
export default function _render() {
  return (
    <View>
      <View style={styles.header}>
        <Text
          style={[
            styles.topic,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {this.props.lan.translation["Interviews"][this.props.language]}
        </Text>
      </View>
      {this.showProgress()}
      <TouchableHighlight
        onPress={() =>
          this.props.navigation.navigate("Category", {
            tid: 28,
            Category: "Interviews"
          })
        }
      >
        <View>
          <Text
            style={[
              styles.viewMore,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["View More"][this.props.language]}{" "}
            <Icon name="arrow-right" size={20} color="#000000" />
          </Text>
        </View>
      </TouchableHighlight>
    </View>
  );
}
