import React, { Component } from "react";
import { View, Text, TouchableHighlight, Image } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
import { baseURL } from "../../config";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
export default class FeaturesBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      news: []
    };
  }

  // componentDidMount = async () => {
  //   await this.fetchNews();
  //   await this.changeRecent();
  // };

  // // get the data in the according language
  // fetchNews = async () => {
  //   let response = await axios.get(
  //     `${baseURL}${this.props.language}/json/category_content?tid=27&front=1&limit=3`
  //     );
  //     await this.setState({
  //       news:response.data
  //     });
  //   };

  //   changeRecent = () => {
  //     const data = { name: "feature", value: this.state.news };
  //     this.props.change(data);
  //   };

  // show the progress the spinner until the data is fetched and then return the data
  showProgress = () => {
    if (this.props.feature.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      // return this.props.feature.map(news => {
      //   return (
      //     <TouchableHighlight
      //       onPress={() =>
      //         this.props.navigation.navigate("SingleNews", { nid: news.nid })
      //       }
      //     >
      //       <View style={styles.header2}>
      //         <View style={styles.image}>
      //           <Image
      //             source={{ uri: news.image }}
      //             style={styles.imageContain}
      //           />
      //         </View>
      //         <Text style={[styles.text,{fontSize : this.props.allFont.fontSizeNormal}]}>{news.title}</Text>
      //       </View>
      //     </TouchableHighlight>
      //   );
      // });
      return (
        <OptimizedFlatList
          data={this.props.feature}
          renderItem={({ item }) => (
            <TouchableHighlight
              onPress={() =>
                this.props.navigation.navigate("SingleNews", { nid: item.nid })
              }
            >
              <View style={styles.header2}>
                <View style={styles.image}>
                  <Image
                    source={{ uri: item.image }}
                    style={styles.imageContain}
                  />
                </View>
                <Text
                  style={[
                    styles.text,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {item.title}
                </Text>
              </View>
            </TouchableHighlight>
          )}
          extraData={this.props.feature}
        />
      );
    }
  };
}
