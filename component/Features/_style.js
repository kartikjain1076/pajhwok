import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-between",
    backgroundColor: "#253C80",
    alignItems: "center",
    marginBottom: 2
  },
  topic: {
    marginLeft: "2%",
    color: "white",
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 0.5,
    paddingBottom: (height / 100) * 0.5
  },
  viewMore: {
    marginRight: "2%",
    color: "#000000",
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 0.5,
    paddingBottom: (height / 100) * 0.5,
    alignSelf: "flex-end"
  },
  header2: {
    display: "flex",
    backgroundColor: "white",
    width: "100%",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: { width: 20, height: 20 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 3,
    marginBottom: 2
  },
  text: {
    width: "68%",
    flexWrap: "wrap",
    alignSelf: "center",
    marginRight: (width / 100) * 2,
    marginLeft: (width / 100) * 2,
    fontFamily: "AverageSans-Regular",
    color: "#000000"
  },
  image: {
    width: "25%",
    marginLeft: "2%",
    marginRight: "2%",
    alignContent: "center",
    marginTop: (height / 100) * 2,
    marginBottom: (height / 100) * 2
  },
  imageContain: {
    height: (height / 100) * 10,
    width: (width / 100) * 20,
    marginLeft: (width / 100) * 2
  },
  progress: {
    marginLeft: (width / 100) * 45,
    marginTop: (height / 100) * 1
  }
});
