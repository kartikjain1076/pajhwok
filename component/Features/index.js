import FeaturesBase from "./_base";
import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

class Features extends FeaturesBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    feature: state.reducer.feature,
    lan: state.reducer.lan,
    allFont: state.reducer.allFont
  };
};

const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Features)
);
