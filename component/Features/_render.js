import React, { Component } from "react";
import { View, Text, TouchableHighlight } from "react-native";
import { styles } from "./_style";
import Icon from "react-native-vector-icons/dist/FontAwesome";
export default function _render() {
  return (
    <View>
      <View style={styles.header}>
        <Text
          style={[
            styles.topic,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {this.props.lan.translation["Features"][this.props.language]}
        </Text>
      </View>
      {this.showProgress()}
      <TouchableHighlight
        onPress={() =>
          this.props.navigation.navigate("Category", {
            tid: 27,
            Category: "Features"
          })
        }
      >
        <View>
          <Text
            style={[
              styles.viewMore,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["View More"][this.props.language]}{" "}
            <Icon name="arrow-right" size={20} color="#000000" />
          </Text>
        </View>
      </TouchableHighlight>
    </View>
  );
}
