import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";
import { Button } from "react-native-paper";
import { styles } from "./_style";
export default function _render() {
  return (
    <View style={styles.wrapper}>
      <View style={styles.textWrapper}>
        <Text style={styles.textStyle}> Font Size</Text>
      </View>
      <View style={styles.buttonWrapper}>
        <Button mode="contained" onPress={this.changeToSmall}>
          Small
        </Button>
      </View>
      <View style={styles.buttonWrapper}>
        <Button mode="contained" onPress={this.changeToMedium}>
          Medium
        </Button>
      </View>
      <View style={styles.buttonWrapper}>
        <Button mode="contained" onPress={this.changeToLarge}>
          Large
        </Button>
      </View>
    </View>
  );
}
