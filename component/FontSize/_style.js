import { StyleSheet, Dimensions } from "react-native";
import { intialState } from "../../Store/Reducer";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  wrapper: {
    display: "flex",
    flex: 1
  },
  textWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 22
  },
  textStyle: {
    fontSize: intialState.allFont.fontSizeNormal,
    fontWeight: "bold"
  },
  textInputWrapper: {
    marginTop: 40,
    flexDirection: "row",
    justifyContent: "center"
  },
  textInputStyle: {
    height: 35,
    borderColor: "grey",
    borderWidth: 1,
    alignSelf: "center",
    width: "80%",
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomColor: "grey"
  },
  buttonWrapper: {
    marginTop: 65,
    width: "76%",
    alignSelf: "center"
  },
  buttonStyle: {
    width: "80%"
  }
});
