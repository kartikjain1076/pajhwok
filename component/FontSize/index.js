import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import FontSizeBase from "./_base";

class FontSize extends FontSizeBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    allFont: state.reducer.allFont,
    drawer: state.reducer.drawer
  };
};
const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(FontSize)
);
