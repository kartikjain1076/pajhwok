import React from "react";
export default class FontSizeBase extends React.PureComponent {
  // change the font size stored in redux to the small
  changeToSmall = async () => {
    const data = {
      name: "allFont",
      value: {
        fontSizeNormal: 14,
        fontSizeTitle: 24,
        fontSizeSmall: 10,
        fontSizeSubTitle: 18
      }
    };
    this.props.change(data);
    this.props.navigation.navigate("Homeee");
  };
  // change the font size stored in redux to the medium
  changeToMedium = async () => {
    const data = {
      name: "allFont",
      value: {
        fontSizeNormal: 16,
        fontSizeTitle: 26,
        fontSizeSmall: 12,
        fontSizeSubTitle: 20
      }
    };
    this.props.change(data);
    this.props.navigation.navigate("Homeee");
  };
  // change the font size stored in redux to the large
  changeToLarge = async () => {
    const data = {
      name: "allFont",
      value: {
        fontSizeNormal: 18,
        fontSizeTitle: 28,
        fontSizeSmall: 14,
        fontSizeSubTitle: 22
      }
    };
    this.props.change(data);
    this.props.navigation.navigate("Homeee");
  };
}
