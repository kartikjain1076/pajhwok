import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import { baseURL } from "../../config";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
export default class OpinonBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      news: []
    };
  }

  // componentDidMount = async () => {
  //   await this.fetchNews();
  //   await this.changeRecent();
  // };

  // changeRecent = () => {
  //   const data = { name: "opinion", value: this.state.news };
  //   this.props.change(data);
  // };
  // fetchNews = async () => {
  //   let response = await axios.get(`${baseURL}${this.props.language}/json/recent_opinion?front=1&limit=3`);
  //   await this.setState({
  //     news: response.data
  //   });
  // };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.props.opinion.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <View style={styles.header2}>
          <View style={styles.image}>
            <Image
              source={require("../../public/opinion.png")}
              style={styles.imageContain}
            />
          </View>
          <View style={{ flexDirection: "column" }}>
            {this.props.opinion.map(news => {
              return (
                <Text
                  style={[
                    styles.text,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {news.title}
                  {"\n"}
                </Text>
              );
            })}
          </View>
        </View>
      );
    }
  };
}
