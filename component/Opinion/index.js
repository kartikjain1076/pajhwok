import OpinonBase from "./_base";
import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

class Opinion extends OpinonBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    opinion: state.reducer.opinion,
    lan: state.reducer.lan,
    allFont: state.reducer.allFont
  };
};

const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Opinion)
);
