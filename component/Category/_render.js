import React, { Component } from "react";
import { View, ScrollView } from "react-native";
import Header from "../header";
import { styles } from "./_style";
export default function _render() {
  return (
    <ScrollView
      stickyHeaderIndices={[0]}
      keyboardShouldPersistTaps="always"
      onScroll={this.handleScroll}
    >
      <Header />
      <View>
        <View style={styles.header}>{this.showCategory()}</View>
        {this.showProgress()}
      </View>
      <View style={styles.progress}>{this.hideProgress()}</View>
    </ScrollView>
  );
}
