import CategoryBase from "./_base";
import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

class Category extends CategoryBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    drawer: state.reducer.drawer,
    allFont: state.reducer.allFont
  };
};

export default withNavigation(connect(mapStateToProps)(Category));
