import React, { Component } from "react";
import { View, Text, TouchableHighlight, Image } from "react-native";
import axios from "axios";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
import { baseURL } from "../../config";
import { OptimizedFlatList } from "react-native-optimized-flatlist";

export default class CategoryBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      limit: 10,
      offset: 0,
      dummy: 0,
      tid: 0,
      end: false,
      Category: ""
    };
  }

  // getting the params that is passed from previous screen
  componentDidMount = async () => {
    this.props.drawer.closeDrawer();
    await this.setState({
      tid: this.props.navigation.getParam("tid"),
      Category: this.props.navigation.getParam("Category")
    });
    if (this.state.Category != "Recent Article") {
      this.fetchNews();
    } else {
      this.recentArticle();
    }
  };

  // update all the required parameter and call the function to update data when we navigate to the same screeen
  shouldComponentUpdate = async (nextProps, nextState) => {
    if (nextProps.navigation.state.params.tid != this.state.tid) {
      await this.setState({
        tid: nextProps.navigation.state.params.tid,
        limit: 10,
        offset: 0,
        dummy: 0,
        news: [],
        end: false,
        Category: nextProps.navigation.getParam("Category")
      });
      if (nextProps.navigation.getParam("Category") != "Recent Article") {
        this.fetchNews();
      } else {
        this.recentArticle();
      }
      this.props.drawer.closeDrawer();
      return true;
    } else return false;
  };

  // get all the data from the backend
  fetchNews = async () => {
    let response = await axios.get(
      `${baseURL}${this.props.language}/json/category_content?tid=${
        this.state.tid
      }&offset=${this.state.offset}&limit=10`
    );
    let temp = this.state.news;
    await response.data.map(data => {
      temp.push(data);
    });
    await this.setState({ news: temp });
    this.setState({ dummy: this.state.dummy + 1 });
    if (response.data.length < 10) this.setState({ end: true });
  };

  // special case! this is called only when a certain button is pressed and decided on the base of params
  recentArticle = async () => {
    let response = await axios.get(
      `${baseURL}${this.props.language}/json/recent_article?offset=${
        this.state.offset
      }&limit=10`
    );
    let temp = this.state.news;
    await response.data.map(data => {
      temp.push(data);
    });
    await this.setState({ news: temp });
    this.setState({ dummy: this.state.dummy + 1 });
    if (response.data.length < 10) this.setState({ end: true });
  };

  // show the progress spinner while the data is getting and then display the data
  showProgress = () => {
    if (this.state.news.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      // return this.state.news.map(news => {
      //   return (
      //     <TouchableHighlight
      //       onPress={() =>
      //         this.props.navigation.navigate("SingleNews", { nid: news.nid })
      //       }
      //     >
      //       <View style={styles.header2}>
      //         <Text style={[styles.text,{fontSize : this.props.allFont.fontSizeNormal}]}>{news.title}</Text>
      //         <View style={styles.image}>
      //           <Image
      //             source={{ uri: news.image }}
      //             style={styles.imageContain}
      //           />
      //         </View>
      //       </View>
      //     </TouchableHighlight>
      //   );
      // });
      return (
        <OptimizedFlatList
          data={this.state.news}
          renderItem={({ item }) => (
            <TouchableHighlight
              onPress={() =>
                this.props.navigation.navigate("SingleNews", { nid: item.nid })
              }
            >
              <View style={styles.header2}>
                <View style={styles.image}>
                  <Image
                    source={{ uri: item.image }}
                    style={styles.imageContain}
                  />
                </View>
                <Text
                  style={[
                    styles.text,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {item.title}
                </Text>
              </View>
            </TouchableHighlight>
          )}
          extraData={this.state}
        />
      );
    }
  };

  // handle the scroll, when reach on end update the limit and offset then call the function to get more data
  handleScroll = async event => {
    if (
      event.nativeEvent.contentOffset.y >=
      event.nativeEvent.contentSize.height -
        event.nativeEvent.layoutMeasurement.height -
        3
    ) {
      await this.setState({ offset: this.state.offset + 10 });
      await this.setState({ limit: this.state.limit + 10 });
      if (this.state.Category != "Recent Article") {
        this.fetchNews();
      } else {
        this.recentArticle();
      }
    }
  };

  // it will show the category of news that has been passed through the params
  showCategory = () => {
    if (this.state.news.length > 0)
      return (
        <View
          style={[
            styles.topic,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          <Text
            style={[
              styles.topic,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.state.Category}
          </Text>
        </View>
      );
  };

  hideProgress = () => {
    if (this.state.end) {
    } else if (this.state.news.length > 0)
      return <Progress.Circle size={30} indeterminate={true} color="#253C80" />;
  };
}
