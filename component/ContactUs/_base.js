import React, { Component } from "react";
import { View, Text } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
import { baseURL } from "../../config";
export default class ContactUsBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      url: `${baseURL}json/page?nid=52`
    };
  }

  // set the url state according to the selected language
  componentDidMount = async () => {
    if (this.props.language == "ps") {
      await this.setState({
        url: `${baseURL}json/page?nid=111596`
      });
    } else if (this.props.language == "dr") {
      await this.setState({
        url: `${baseURL}json/page?nid=111595`
      });
    }
    this.fetchData();
  };

  //get the data from the selected url
  fetchData = async () => {
    let response = await axios.get(this.state.url);
    await this.setState({
      data: [{ title: response.data[0].title, body: response.data[0].body }]
    });
  };

  // show the progress spinner while getting the data and then show the data
  showProgress = () => {
    if (this.state.data.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <View style={styles.main}>
          <Text
            style={[
              styles.title,
              { fontSize: this.props.allFont.fontSizeTitle }
            ]}
          >
            {this.state.data[0].title}
          </Text>
          <HTML html={this.state.data[0].body} containerStyle={styles.body} />
        </View>
      );
    }
  };
}
