import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  main: {
    marginBottom: (height / 100) * 1,
    marginTop: (height / 100) * 1,
    marginLeft: (width / 100) * 2,
    marginRight: (width / 100) * 2,
    backgroundColor: "#ffffff",
    shadowColor: "#000",
    shadowOffset: { width: 20, height: 20 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 3
    // height : '98%'
  },
  background: {
    width: "100%",
    backgroundColor: "#d3d3d3"
  },
  title: {
    fontFamily: "AverageSans-Regular",
    marginTop: (height / 100) * 5,
    marginBottom: (height / 100) * 5,
    marginLeft: (width / 100) * 3
  },
  body: {
    marginLeft: (width / 100) * 3,
    marginRight: (width / 100) * 3,
    flexWrap: "wrap",
    height: "100%",
    width: "94%"
  },
  progress: {
    marginLeft: (width / 100) * 45,
    marginTop: (height / 100) * 1
  }
});
