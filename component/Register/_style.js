import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  main: {
    height: (height / 100) * 97,
    width: width,
    backgroundColor: "#000000",
    alignItems: "center",
    justifyContent: "space-around"
  },
  imageContainer: {
    height: (height / 100) * 20,
    width: (width / 100) * 40
  },
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    color: "#ffffff",
    fontFamily: "AverageSans-Regular"
  },
  formContainer: {
    height: (height / 100) * 40,
    width: (width / 100) * 80,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "space-around"
  },
  formText: {
    fontFamily: "AverageSans-Regular",
    color: "#000000",
    marginTop: (height / 100) * 2
  },
  textField: {
    width: "80%",
    height: "20%",
    backgroundColor: "#ffffff"
  },
  signupButton: {
    width: "80%",
    backgroundColor: "#253C80",
    marginBottom: (height / 100) * 2
  },
  signupButtonText: {
    color: "#ffffff",
    fontFamily: "AverageSans-Regular"
  },
  bottomButton: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around"
  },
  signinButton: {
    backgroundColor: "#253C80"
  },
  forgotpasswordButton: {
    backgroundColor: "#253C80"
  },
  signinButtonText: {
    color: "#ffffff",
    fontFamily: "AverageSans-Regular"
  },
  forgotpasswordButtonText: {
    color: "#ffffff",
    fontFamily: "AverageSans-Regular"
  },
  errorText: {
    fontWeight: "bold",
    color: "red"
  },

  errorFields: {
    borderColor: "red"
  }
});
