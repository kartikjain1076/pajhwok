import React, { Component } from "react";
import { View, ScrollView, Text, Image } from "react-native";
import { TextInput, Button } from "react-native-paper";
import { styles } from "./_style";
export default function _render() {
  return (
    <ScrollView keyboardShouldPersistTaps="always">
      <View style={styles.main}>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/pajhwok.png")}
            style={styles.image}
          />
        </View>
        <View>
          <Text
            style={[
              styles.text,
              { fontSize: this.props.allFont.fontSizeTitle }
            ]}
          >
            {
              this.props.lan.translation["Pajhwok Afghan News"][
                this.props.language
              ]
            }
          </Text>
        </View>
        <View style={styles.formContainer}>
          <Text
            style={[
              styles.formText,
              { fontSize: this.props.allFont.fontSizeSubTitle }
            ]}
          >
            {this.props.lan.translation["SignUp"][this.props.language]}
          </Text>
          <TextInput
            label={this.props.lan.translation["Name"][this.props.language]}
            isRequired={true}
            value={this.state.name}
            returnKeyType="next"
            onSubmitEditing={() => this.secondTextInput.focus()}
            onChangeText={text => this.setState({ name: text })}
            style={[
              styles.textField,
              this.state.usernameError ? styles.errorFields : ""
            ]}
            mode="flat"
            underlineColor="#253C80"
            value={this.state.name}
          />
          <Text style={[styles.errorFields, styles.errorText]}>
            {this.state.usernameError}
          </Text>
          <TextInput
            label={this.props.lan.translation["Email"][this.props.language]}
            style={[
              styles.textField,
              this.state.emailError ? styles.errorFields : ""
            ]}
            mode="flat"
            underlineColor="#253C80"
            keyboardType="email-address"
            isRequired={true}
            value={this.state.email}
            ref={input => {
              this.secondTextInput = input;
            }}
            onChangeText={text => this.setState({ email: text })}
          />
          <Text style={[styles.errorFields, styles.errorText]}>
            {this.state.emailError}
          </Text>
          <Button style={styles.signupButton} onPress={this.checkValidation}>
            <Text style={styles.signupButtonText}>
              {this.props.lan.translation["SignUp"][this.props.language]}
            </Text>
          </Button>
        </View>
        <View style={styles.bottomButton}>
          <Button
            style={styles.signinButton}
            onPress={() => this.props.navigation.goBack()}
          >
            <Text style={styles.signinButtonText}>
              {this.props.lan.translation["SignIn"][this.props.language]}
            </Text>
          </Button>
          <Button
            style={styles.forgotpasswordButton}
            onPress={() => this.props.navigation.navigate("ForgotPassword")}
          >
            <Text style={styles.forgotpasswordButtonText}>
              {
                this.props.lan.translation["Forgot Password"][
                  this.props.language
                ]
              }
            </Text>
          </Button>
        </View>
      </View>
    </ScrollView>
  );
}
