import RegisterBase from "./_base";
import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

class Register extends RegisterBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    photo: state.reducer.photo,
    lan: state.reducer.lan,
    allFont: state.reducer.allFont
  };
};

export default withNavigation(connect(mapStateToProps)(Register));
