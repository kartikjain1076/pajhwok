import React, { Component } from "react";
import { View, Text } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
export default class RegisterBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      usernameError: "",
      value: "",
      email: "",
      emailError: ""
    };
  }

  handleTextChange = newText => this.setState({ value: newText });
  // validate the data that is being entered in the text input
  validateRegister = text => {
    let textUsername = this.state.name;
    let textEmail = this.state.email;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let usernameResult = reg.test(textEmail);
    if (textUsername === "" && textEmail === "") {
      this.setState({
        usernameError: this.props.lan.translation["Username is Empty"][
          this.props.language
        ],
        emailError: this.props.lan.translation["Email Field is Empty"][
          this.props.language
        ]
      });
      return false;
    } else if (textUsername === "" && usernameResult == false) {
      this.setState({
        usernameError: this.props.lan.translation["Username is Empty"][
          this.props.language
        ],
        emailError: this.props.lan.translation["Email Pattern is Wrong"][
          this.props.language
        ]
      });

      return false;
    } else if (textUsername === "" && usernameResult == true) {
      this.setState({
        usernameError: this.props.lan.translation["Username is Empty"][
          this.props.language
        ],
        emailError: ""
      });
      return false;
    } else if (textEmail === "" && this.state.name.length > 0) {
      this.setState({
        usernameError: "",
        emailError: this.props.lan.translation["Email Field is Empty"][
          this.props.language
        ]
      });
      return false;
    } else if (usernameResult == false && this.state.name.length > 0) {
      this.setState({
        emailError: this.props.lan.translation["Email Pattern is Wrong"][
          this.props.language
        ],
        usernameError: ""
      });
      return false;
    } else {
      this.setState({ usernameError: "", emailError: "" });
      return true;
    }
  };

  checkValidation = () => {
    const a = this.validateRegister();
    if (a) {
    } else {
    }
  };
}
