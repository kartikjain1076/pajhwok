import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-between",
    backgroundColor: "#253C80",
    alignItems: "center",
    marginBottom: 2
  },
  topic: {
    marginLeft: "2%",
    color: "#ffffff",
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 1,
    paddingBottom: (height / 100) * 1
  },
  viewMore: {
    marginRight: "2%",
    color: "#000000",
    fontFamily: "AverageSans-Regular",
    paddingTop: (height / 100) * 1,
    paddingBottom: (height / 100) * 1,
    alignSelf: "flex-end"
  },
  header2: {
    display: "flex",
    backgroundColor: "white",
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    paddingBottom: (height / 100) * 2,
    paddingTop: (height / 100) * 2,
    flexWrap: "wrap"
  },
  imageContainer: {
    height: (height / 100) * 15,
    width: width > 420 ? (width / 100) * 20 : (width / 100) * 40
  },
  image1: {
    height: (height / 100) * 15,
    width: width > 420 ? (width / 100) * 20 : (width / 100) * 40,
    position: "absolute"
  },
  image2: {
    height: (height / 100) * 5,
    width: (width / 100) * 10,
    position: "absolute",
    left: (width / 100) * 5,
    top: (height / 100) * 4
  },
  content: {
    flexDirection: "column",
    width: width > 420 ? (width / 100) * 20 : (width / 100) * 40
  },
  progress: {
    marginLeft: (width / 100) * 45,
    marginTop: (height / 100) * 1,
    marginBottom: (height / 100) * 1
  },
  text: {
    // flexWrap : 'wrap',
    fontFamily: "AverageSans-Regular",
    alignSelf: "center",
    color: "#000000"
  }
});
