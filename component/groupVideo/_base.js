import React, { Component } from "react";
import { View, Text, Image, TouchableHighlight } from "react-native";
import axios from "axios";
import * as Progress from "react-native-progress";
import { styles } from "./_style";
import { baseURL } from "../../config";
import { intialState } from "../../Store/Reducer";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
export default class GroupVideoBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      offset: 0
    };
  }

  componentDidMount = async () => {
    this.props.drawer.closeDrawer();
    await this.fetchNews();
  };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.state.news.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      //   return(<View>{this.state.news.map(newss => {
      //       return(<View style={styles.header2}>{newss.map(news => {
      //           return (
      //             <TouchableHighlight onPress = {() => this.props.navigation.navigate("SingleVideo", { nid: news.nid })}>
      //             <View style={styles.content}>
      //               <View style={styles.imageContainer}>
      //                 <Image source={{ uri: news.url }} style={styles.image1} />
      //                 <Image
      //                   source={require("../../public/video.png")}
      //                   style={styles.image2}
      //                 />
      //               </View>
      //               <Text style={{ flexWrap: "wrap" , fontSize : intialState.allFont.fontSizeNormal}}>{news.title}</Text>
      //             </View>
      //           </TouchableHighlight>)})}
      //             </View>
      //             );}
      //   )}<View style={styles.progress}>
      //   <Progress.Circle size={30} indeterminate={true} color="#253C80" />
      // </View>
      // </View>)

      return (
        <View>
          <OptimizedFlatList
            data={this.state.news}
            renderItem={({ item }) => (
              <View style={styles.header2}>
                {item.map(news => {
                  return (
                    <TouchableHighlight
                      onPress={() =>
                        this.props.navigation.navigate("SingleVideo", {
                          nid: news.nid
                        })
                      }
                    >
                      <View style={styles.content}>
                        <View style={styles.imageContainer}>
                          <Image
                            source={{ uri: news.url }}
                            style={styles.image1}
                          />
                          <Image
                            source={require("../../public/video.png")}
                            style={styles.image2}
                          />
                        </View>
                        <Text
                          style={[
                            styles.text,
                            { fontSize: intialState.allFont.fontSizeNormal }
                          ]}
                        >
                          {news.title}
                        </Text>
                      </View>
                    </TouchableHighlight>
                  );
                })}
              </View>
            )}
          />
          <View style={styles.progress}>
            <Progress.Circle size={30} indeterminate={true} color="#253C80" />
          </View>
        </View>
      );
    }
  };

  // fetch the data according to the language
  fetchNews = async () => {
    let response = await axios.get(
      `${baseURL}${this.props.language}/json/new_video_content?front=1&offset=${
        this.state.offset
      }&limit=12`
    );
    let temp = [];
    let temp2 = this.state.news;
    response.data.map(async aaa => {
      temp.push(aaa);
      if (temp.length == 4) {
        temp2 = [...temp2, temp];
        temp = [];
      }
    });
    await this.setState({ news: temp2 });
  };
  // detect the scroll position and on the end of scroll change the offset state and fetch the data according to it
  handleScroll = async event => {
    if (
      event.nativeEvent.contentOffset.y >=
      event.nativeEvent.contentSize.height -
        event.nativeEvent.layoutMeasurement.height -
        3
    ) {
      await this.setState({ offset: this.state.offset + 12 });
      this.fetchNews();
    }
  };
}
