import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import GroupVideoBase from "./_base";

class GroupVideo extends GroupVideoBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}
const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    video: state.reducer.video,
    lan: state.reducer.lan,
    drawer: state.reducer.drawer,
    allFont: state.reducer.allFont
  };
};

const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(GroupVideo)
);
