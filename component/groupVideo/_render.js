import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import Header from "../header";

import { styles } from "./_style";
export default function _render() {
  return (
    <ScrollView
      stickyHeaderIndices={[0]}
      keyboardShouldPersistTaps="always"
      onScroll={this.handleScroll}
    >
      <Header />
      <View style={styles.header}>
        <Text
          style={[
            styles.topic,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {this.props.lan.translation["Recent Videos"][this.props.language]}
        </Text>
      </View>
      {this.showProgress()}
    </ScrollView>
  );
}
