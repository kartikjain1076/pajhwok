import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  main: {
    height: (height / 100) * 5,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row"
  },
  input: {
    width: "90%",
    height: "120%"
  },
  searchContainer: {
    width: "85%",
    backgroundColor: "#d3d3d3",
    marginLeft: "2%",
    height: "80%",
    borderRadius: 5,
    flexDirection: "row"
  },
  searchIcon: {
    width: "5%",
    height: "100%",
    justifyContent: "center",
    marginLeft: "1%"
  },
  arrow: {
    justifyContent: "center"
  }
});
