import React from "react";
import { View, TextInput, TouchableHighlight } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import { styles } from "./_style";
export default function _render() {
  return (
    <View style={styles.main}>
      <View style={styles.searchContainer}>
        <View style={styles.searchIcon}>
          <Icon name="search" size={18} color="#696969" />
        </View>
        <TextInput
          placeholder={
            this.props.lan.translation["Search"][this.props.language]
          }
          style={styles.input}
          onChangeText={value => this.setState({ Content: value })}
          onSubmitEditing={data => this.keyPress(data)}
          returnKeyType="search"
        />
      </View>
      <View style={styles.arrow}>
        <TouchableHighlight
          onPress={() =>
            this.props.navigation.navigate("Search", {
              Content: this.state.Content
            })
          }
        >
          <Icon name="long-arrow-right" size={22} color="#696969" />
        </TouchableHighlight>
      </View>
    </View>
  );
}
