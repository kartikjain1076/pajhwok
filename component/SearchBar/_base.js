import React from "react";

export default class SearchBarBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      Content: ""
    };
  }

  //detect the enter key and navigate to search screen
  keyPress = data => {
    this.props.navigation.navigate("Search", { Content: this.state.Content });
  };
}
