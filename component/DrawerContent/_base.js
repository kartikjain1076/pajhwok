import React, { Component } from "react";
import SplashScreen from "react-native-splash-screen";
import Axios from "axios";
export default class DrawerContentBase extends React.PureComponent {
  componentDidMount = async () => {
    await this.fetchCategory();
  };

  // get the all categories.
  fetchCategory = async () => {
    let response = await Axios.get("https://www.pajhwok.com/json/category");
    await this.props.change({ name: "allCategory", value: response.data });
  };
}
