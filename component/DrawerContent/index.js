import DrawerContentBase from "./_base";
import _render from "./_render";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

class DrawerContent extends DrawerContentBase {
  render() {
    return _render.call(this, this.state, this.props);
  }
}

const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    lan: state.reducer.lan,
    childDrawer: state.reducer.childDrawer,
    allFont: state.reducer.allFont,
    allCategory: state.reducer.allCategory
  };
};
const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DrawerContent)
);
