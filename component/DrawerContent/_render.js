import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableHighlight,
  Image,
  Keyboard
} from "react-native";
import { OptimizedFlatList } from "react-native-optimized-flatlist";
import { styles } from "./_style";
import Icon from "react-native-vector-icons/dist/Octicons";
export default function _render() {
  Keyboard.dismiss();
  return (
    <View style={styles.main}>
      <View style={styles.scrollviewContainer}>
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.listItem}>
              <Icon name="primitive-dot" size={20} color="#5e5d5d" />
              <TouchableHighlight
                onPress={() => this.props.childDrawer.navigate("Homeee")}
              >
                <Text
                  style={[
                    styles.listText,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {this.props.lan.translation["Home"][this.props.language]}
                </Text>
              </TouchableHighlight>
            </View>
            <View style={styles.titleContainer}>
              <Text
                style={[
                  styles.title,
                  { fontSize: this.props.allFont.fontSizeNormal }
                ]}
              >
                {
                  this.props.lan.translation["News Categories"][
                    this.props.language
                  ]
                }
              </Text>
            </View>

            {/* <OptimizedFlatList
      data = {this.props.allCategory}
      renderItem = {({item}) => <View style = {styles.listItem}>
      <Icon name="primitive-dot" size={20} color="#5e5d5d" />
      <TouchableHighlight onPress={() => this.props.childDrawer.navigate("Category", {tid : item.tid,Category : item.category_name[this.props.language]})}>
      <Text style = {[styles.listText,{fontSize : this.props.allFont.fontSizeNormal}]}>{item.category_name[this.props.language]}</Text>
      </TouchableHighlight>
      </View>} 
      extraData = {this.props.language}
      extraData = {this.props.allCategory}
      /> */}

            {this.props.allCategory.map(item => {
              return (
                <View style={styles.listItem}>
                  <Icon name="primitive-dot" size={20} color="#5e5d5d" />
                  <TouchableHighlight
                    onPress={() =>
                      this.props.childDrawer.navigate("Category", {
                        tid: item.tid,
                        Category: item.category_name[this.props.language]
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.listText,
                        { fontSize: this.props.allFont.fontSizeNormal }
                      ]}
                    >
                      {item.category_name[this.props.language]}
                    </Text>
                  </TouchableHighlight>
                </View>
              );
            })}

            <View style={styles.titleContainer}>
              <Text
                style={[
                  styles.title,
                  { fontSize: this.props.allFont.fontSizeNormal }
                ]}
              >
                {
                  this.props.lan.translation["Pajhwok Services"][
                    this.props.language
                  ]
                }
              </Text>
            </View>
            <View style={styles.listItem}>
              <Icon name="primitive-dot" size={20} color="#5e5d5d" />
              <TouchableHighlight
                onPress={() => this.props.childDrawer.navigate("GroupVideo")}
              >
                <Text
                  style={[
                    styles.listText,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {
                    this.props.lan.translation["Video Service"][
                      this.props.language
                    ]
                  }
                </Text>
              </TouchableHighlight>
            </View>
            <View style={styles.listItem}>
              <Icon name="primitive-dot" size={20} color="#5e5d5d" />
              <TouchableHighlight
                onPress={() => this.props.childDrawer.navigate("GroupImage")}
              >
                <Text
                  style={[
                    styles.listText,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {
                    this.props.lan.translation["Photo Service"][
                      this.props.language
                    ]
                  }
                </Text>
              </TouchableHighlight>
            </View>
            <View style={styles.listItem}>
              <Icon name="primitive-dot" size={20} color="#5e5d5d" />
              <TouchableHighlight
                onPress={() => this.props.childDrawer.navigate("Login")}
              >
                <Text
                  style={[
                    styles.listText,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {
                    this.props.lan.translation["Audio Service"][
                      this.props.language
                    ]
                  }
                </Text>
              </TouchableHighlight>
            </View>
            <View style={styles.listItem}>
              <Icon name="primitive-dot" size={20} color="#5e5d5d" />
              <TouchableHighlight
                onPress={() => this.props.childDrawer.navigate("Login")}
              >
                <Text
                  style={[
                    styles.listText,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {this.props.lan.translation["Opinions"][this.props.language]}
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </View>
      <View style={styles.bottomImage}>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/facebook.png")}
            style={styles.image}
          />
        </View>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/twitter.png")}
            style={styles.image}
          />
        </View>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/vimeo.png")}
            style={styles.image}
          />
        </View>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/linkedin.png")}
            style={styles.image}
          />
        </View>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/youtube.png")}
            style={styles.image}
          />
        </View>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../public/google.png")}
            style={styles.image}
          />
        </View>
      </View>
    </View>
  );
}
