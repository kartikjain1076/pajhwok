import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import { TextField } from "react-native-material-textfield";
import { Button } from "react-native-paper";
import { styles } from "./_style";
export default function _render() {
  let { email } = this.state;

  return (
    <View>
      <View style={styles.signForm}>
        <View style={styles.forgotTextDiv}>
          <Text
            style={[
              styles.forgotText,
              { fontSize: this.props.allFont.fontSizeTitle }
            ]}
          >
            {this.props.lan.translation["Forgot Password"][this.props.language]}
          </Text>
        </View>
        <View style={styles.forgotPasswordFieldDiv}>
          <TextField
            autoCorrect={false}
            error={this.state.emailError}
            label={this.props.lan.translation["Email"][this.props.language]}
            value={email}
            onChangeText={email => this.setState({ email: email })}
          />
          <Button
            style={styles.forgotButton}
            mode="contained"
            onPress={this.checkValidation}
          >
            Submit
          </Button>
        </View>
      </View>
    </View>
  );
}
