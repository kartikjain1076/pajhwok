import React, { Component } from "react";
export default class ForgotPasswordBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      emailError: ""
    };
  }

  // validate all the value that is entered in the input fields
  validateRegister = () => {
    let textEmail = this.state.email;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let usernameResult = reg.test(textEmail);
    if (textEmail === "") {
      this.setState({
        emailError: "Email Field is empty"
      });
      return false;
    } else if (usernameResult == false) {
      this.setState({
        emailError: "Invalid Email"
      });

      return false;
    } else if (usernameResult == true) {
      this.setState({
        emailError: ""
      });
      return false;
    }
  };
  checkValidation = () => {
    const a = this.validateRegister();
    if (a) {
    } else {
    }
  };
}
