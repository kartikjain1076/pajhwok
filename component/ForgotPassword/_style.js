import { StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
export const styles = StyleSheet.create({
  forgotPasswordFieldDiv: {},
  errorText: {
    marginLeft: "20%",
    fontWeight: "bold",
    marginBottom: "3%"
  },

  forgotText: {
    fontWeight: "bold",
    color: "#013243"
  },
  forgotTextDiv: {
    alignItems: "center",
    marginTop: "7%",
    marginBottom: "12%"
  },
  forgotButton: {
    backgroundColor: "#1f3a93"
  },

  signForm: {
    borderRadius: 5,
    marginLeft: "8%",
    marginRight: "8%"
  }
});
