import React, { Component } from "react";
import { View, ScrollView, Text, TouchableHighlight } from "react-native";
import Header from "../header";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import { styles } from "./_style";
export default function _render() {
  return (
    <View>
      <View style={styles.header}>
        <Text
          style={[
            styles.topic,
            { fontSize: this.props.allFont.fontSizeNormal }
          ]}
        >
          {this.props.lan.translation["Recent Article"][this.props.language]}
        </Text>
      </View>
      {this.showProgress()}
      <View>
        <TouchableHighlight
          onPress={() =>
            this.props.navigation.navigate("Category", {
              Category: "Recent Article"
            })
          }
        >
          <Text
            style={[
              styles.viewMore,
              { fontSize: this.props.allFont.fontSizeNormal }
            ]}
          >
            {this.props.lan.translation["View More"][this.props.language]}{" "}
            <Icon name="arrow-right" size={20} color="#000000" />
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}
