import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableHighlight,
  FlatList
} from "react-native";
import axios from "axios";
import * as Progress from "react-native-progress";
import { baseURL } from "../../config";
import { styles } from "./_style";
import { OptimizedFlatList } from "react-native-optimized-flatlist";

export default class Recent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      news: []
    };
  }

  // componentDidMount = async() => {
  //     await this.fetchNews();
  //     await this.changeRecent();

  // }

  // changeRecent=()=>{

  //     const data = {name : 'recent', value : this.state.news};
  //      this.props.change(data);
  //   }

  // fetchNews = async() => {
  //     let response = await axios.get(`${baseURL}${this.props.language}/json/recent_article?front=1&limit=5`);
  //     await this.setState({news :response.data});
  // }

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.props.recent.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
          {/* <Spinner type = 'ThreeBounce' color = '#253C80' /> */}
        </View>
      );
    } else {
      // return(this.props.recent.map((news) => {
      //     return(
      //     <TouchableHighlight onPress = {() => this.props.navigation.navigate('SingleNews',{nid : news.nid})}>
      // <View
      //     style={styles.header2}>
      //       <View style = {styles.image}>
      //       <Image source = {{uri : news.image}} style = {styles.imageContain} />
      //       </View>
      //       <Text style = {[styles.text,{fontSize : this.props.allFont.fontSizeNormal}]}>{news.title}</Text>
      //   </View>
      //   </TouchableHighlight>
      // )}))
      return (
        <OptimizedFlatList
          data={this.props.recent}
          renderItem={({ item }) => (
            <TouchableHighlight
              onPress={() =>
                this.props.navigation.navigate("SingleNews", { nid: item.nid })
              }
            >
              <View style={styles.header2}>
                <View style={styles.image}>
                  <Image
                    source={{ uri: item.image }}
                    style={styles.imageContain}
                  />
                </View>
                <Text
                  style={[
                    styles.text,
                    { fontSize: this.props.allFont.fontSizeNormal }
                  ]}
                >
                  {item.title}
                </Text>
              </View>
            </TouchableHighlight>
          )}
          extraData={this.props.recent}
        />
      );
    }
  };
}
