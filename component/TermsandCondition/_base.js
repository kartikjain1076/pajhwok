import React, { Component } from "react";
import { View, Text } from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";
import * as Progress from "react-native-progress";
import { baseURL } from "../../config";
import { styles } from "./_style";
export default class TermsandConditionBase extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      url: `${baseURL}json/page?nid=246463`
    };
  }

  componentDidMount = async () => {
    if (this.props.language == "ps")
      await this.setState({
        url: `${baseURL}json/page?nid=246465`
      });
    else if (this.props.language == "dr")
      await this.setState({
        url: `${baseURL}json/page?nid=246464`
      });
    this.fetchData();
  };

  // fetch the data according to the language
  fetchData = async () => {
    let response = await axios.get(this.state.url);
    await this.setState({
      data: [{ title: response.data[0].title, body: response.data[0].body }]
    });
  };

  // return the progress spinner while data is being fetched and after that return the data.
  showProgress = () => {
    if (this.state.data.length == 0) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <View style={styles.main}>
          <Text
            style={[
              styles.title,
              { fontSize: this.props.allFont.fontSizeTitle }
            ]}
          >
            {this.state.data[0].title}
          </Text>
          <HTML html={this.state.data[0].body} containerStyle={styles.body} />
        </View>
      );
    }
  };
}
