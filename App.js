import {createStackNavigator} from 'react-navigation';
import Drawer from "./Drawer";
import Login from './component/Login/index';
import Register from './component/Register/index';
import ForgotPassword from './component/ForgotPassword/index';

const StackNav = createStackNavigator({
  Drawer:  Drawer,
  Login : Login,
  Register : Register,
  ForgotPassword : ForgotPassword
},
{
  intialRouteName : 'Drawer',
  navigationOptions: {
    header: null,
  }
}
);


export default StackNav;