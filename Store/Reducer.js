import English from "../Language/English";

export const intialState = {
  language: "en",
  recent: [],
  feature: [],
  interview: [],
  video: [],
  opinion: [],
  photo: [],
  slideshow: [],
  lan: English,
  drawer: {},
  childDrawer: {},
  allFont: {
    fontSizeNormal: 16,
    fontSizeTitle: 26,
    fontSizeSmall: 12,
    fontSizeSubTitle: 20
  },
  allCategory: [
    {
      tid: "16",
      category_name: {
        ps: "\u067e\u06d0\u069a\u06d0",
        dr: "\u062d\u0648\u0627\u062f\u062b",
        en: "Accidents & Disasters"
      }
    },
    {
      tid: "19",
      category_name: {
        ps: "\u06a9\u0631\u0646\u0647",
        dr: "\u0632\u0631\u0627\u0639\u062a",
        en: "Agriculture"
      }
    },
    {
      tid: "17",
      category_name: {
        ps:
          "\u0633\u0648\u062f\u0627\u06ab\u0631\u064a \u0627\u0648 \u0627\u0642\u062a\u0635\u0627\u062f",
        dr:
          "\u062a\u062c\u0627\u0631\u062a \u0648 \u0627\u0642\u062a\u0635\u0627\u062f",
        en: "Business & Economics"
      }
    },
    {
      tid: "17154",
      category_name: {
        ps:
          "\u062f \u0689\u06d0\u067c\u0627 \u0698\u0648\u0631\u0646\u0627\u0644\u06d0\u0696\u0645 \u0631\u0627\u067e\u0648\u0631\u0648\u0646\u0647",
        dr:
          "\u06af\u0632\u0627\u0631\u0634 \u0647\u0627\u06cc \u062f\u06cc\u062a\u0627 \u0698\u0648\u0631\u0646\u0627\u0644\u06cc\u0632\u0645",
        en: "Data Journalism Stories"
      }
    },
    {
      tid: "21",
      category_name: {
        ps:
          "\u069a\u0648\u0648\u0646\u0647 \u0627\u0648 \u0631\u0648\u0632\u0646\u0647",
        dr:
          "\u062a\u0639\u0644\u06cc\u0645 \u0648 \u062a\u0631\u0628\u06cc\u0647",
        en: "Education"
      }
    },
    {
      tid: "1037",
      category_name: {
        ps:
          "\u0686\u0627\u067e\u06d0\u0631\u064a\u0627\u0644 \u0633\u0627\u062a\u0646\u0647",
        dr: "\u0645\u062d\u064a\u0637 \u0632\u064a\u0633\u062a",
        en: "Environment"
      }
    },
    {
      tid: "27",
      category_name: {
        ps: "\u0641\u064a\u0686\u0631\u0648\u0646\u0647",
        dr: "\u0641\u064a\u0686\u0631\u0647\u0627",
        en: "Features"
      }
    },
    {
      tid: "12",
      category_name: {
        ps:
          "\u062f\u0648\u0644\u062a \u0627\u0648 \u0633\u064a\u0627\u0633\u062a",
        dr: "\u062f\u0648\u0644\u062a \u0648 \u0633\u064a\u0627\u0633\u062a",
        en: "Governance & Politics"
      }
    },
    {
      tid: "20",
      category_name: {
        ps: "\u0631\u0648\u063a\u062a\u06cc\u0627",
        dr: "\u0635\u062d\u062a",
        en: "Health"
      }
    },
    {
      tid: "1036",
      category_name: {
        ps:
          "\u0646\u0648\u069a\u062a \u0627\u0648 \u067c\u06a9\u0646\u0627\u0644\u0648\u062c\u064a",
        dr:
          "\u0646\u0648\u0627\u0648\u0631\u0649 \u0648 \u062a\u06a9\u0646\u0627\u0644\u0648\u062c\u0649",
        en: "Innovation & Technology"
      }
    },
    {
      tid: "28",
      category_name: {
        ps: "\u0645\u0631\u06a9\u06d0",
        dr: "\u0645\u0635\u0627\u062d\u0628\u0647 \u0647\u0627",
        en: "Interviews"
      }
    },
    {
      tid: "13",
      category_name: {
        ps:
          "\u0631\u0633\u0646\u064a\u0632\u06d0 \u062e\u0628\u0631\u062a\u064a\u0627\u0648\u06d0",
        dr:
          "\u0627\u0639\u0644\u0627\u0645\u064a\u0647 \u0647\u0627\u06cc \u0645\u0637\u0628\u0648\u0639\u0627\u062a\u06cc",
        en: "Media Releases"
      }
    },
    {
      tid: "22",
      category_name: {
        ps: "\u06a9\u0689\u0648\u0627\u0644 ",
        dr: "\u0645\u0647\u0627\u062c\u0631\u064a\u0646",
        en: "Migration"
      }
    },
    {
      tid: "18",
      category_name: {
        ps: "\u0628\u064a\u0627\u0631\u063a\u0648\u0646\u0647",
        dr: "\u0628\u0627\u0632\u0633\u0627\u0632\u0649",
        en: "Reconstruction"
      }
    },
    {
      tid: "23",
      category_name: {
        ps: "\u062f\u06cc\u0646 \u0627\u0648 \u06a9\u0644\u062a\u0648\u0631",
        dr: "\u062f\u06cc\u0646 \u0648 \u0641\u0631\u0647\u0646\u06af",
        en: "Religion & Culture"
      }
    },
    {
      tid: "15",
      category_name: {
        ps:
          "\u0627\u0645\u0646\u06cc\u062a \u0627\u0648 \u062c\u0631\u0645\u0648\u0646\u0647",
        dr:
          "\u0627\u0645\u0646\u06cc\u062a \u0648 \u062c\u0631\u0627\u064a\u0645",
        en: "Security & Crime"
      }
    },
    {
      tid: "24",
      category_name: {
        ps: "\u067c\u0648\u0644\u0646\u0647",
        dr: "\u0627\u062c\u062a\u0645\u0627\u0639",
        en: "Society"
      }
    },
    {
      tid: "25",
      category_name: {
        ps: "\u0644\u0648\u0628\u06d0",
        dr: "\u0648\u0631\u0632\u0634",
        en: "Sports"
      }
    },
    {
      tid: "14",
      category_name: {
        ps: "\u069a\u0681\u06d0",
        dr: "\u0632\u0646\u0627\u0646",
        en: "Women"
      }
    },
    {
      tid: "42155",
      category_name: {
        ps:
          "\u0646\u0693\u06cc\u0648\u0627\u0644 \u062e\u0628\u0631\u0648\u0646\u0647",
        dr: "\u0627\u062e\u0628\u0627\u0631 \u062c\u0647\u0627\u0646",
        en: "World News"
      }
    }
  ]
};

const Reducer = (state = intialState, action) => {
  return {
    ...state,
    [action.type]: action.value
  };
};

export default Reducer;
