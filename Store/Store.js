import { createStore, combineReducers } from "redux";
import Reducer from "./Reducer";

const rootReducer = combineReducers({
  reducer: Reducer
});

const Store = createStore(rootReducer);

export default Store;
