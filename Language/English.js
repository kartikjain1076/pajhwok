//default language and there translation in case if the data is not received from backend
const English = {
  translation: {
    PAJHWOK: { en: "PAJHWOK", dr: "PAJHWOK", ps: "PAJHWOK", translate: false },
    "AFGHAN NEWS": {
      en: "AFGHAN NEWS",
      dr: "AFGHAN NEWS",
      ps: "AFGHAN NEWS",
      translate: false
    },
    Search: {
      en: "Search",
      dr: "\u062c\u0633\u062a\u062c\u0648",
      ps: "\u0644\u067c\u0648\u0646",
      translate: true
    },
    "Recent Article": {
      en: "Recent Article",
      dr: "Recent Article",
      ps: "Recent Article",
      translate: false
    },
    "View More": {
      en: "View More",
      dr: "\u0633\u0627\u06cc\u0631\u06af\u0632\u0627\u0631\u0634",
      ps: "\u0646\u0648\u0631 \u062e\u0628\u0631\u0648\u0646\u0647",
      translate: true
    },
    Features: {
      en: "Features",
      dr: " \u0641\u064a\u0686\u0631\u0647\u0627",
      ps: "\u0641\u064a\u0686\u0631\u0648\u0646\u0647",
      translate: true
    },
    Interviews: {
      en: "Interviews",
      dr: "\u0645\u0635\u0627\u062d\u0628\u0647 \u0647\u0627",
      ps: "\u0645\u0631\u06a9\u06d0",
      translate: true
    },
    "Recent Videos": {
      en: "Recent Videos",
      dr:
        "\u0648\u064a\u062f\u064a\u0648\u0647\u0627\u06cc \u062c\u062f\u064a\u062f",
      ps:
        "\u0646\u0648\u06d0 \u0648\u064a\u0689\u064a\u0648\u06ab\u0627\u0646\u06d0",
      translate: true
    },
    "Recent Photos": {
      en: "Recent Photos",
      dr: "\u0639\u06a9\u0633 \u0647\u0627\u06cc \u062c\u062f\u064a\u062f ",
      ps: "\u0646\u0648\u064a \u0627\u0646\u0681\u0648\u0631\u0648\u0646\u0647",
      translate: true
    },
    Opinions: {
      en: "Opinions",
      dr: "\u0645\u0642\u0627\u0644\u0627\u062a",
      ps: "\u0644\u064a\u06a9\u0646\u06d0",
      translate: true
    },
    Home: {
      en: "Home",
      dr: "\u0635\u0641\u062d\u0647 \u0627\u0635\u0644\u06cc",
      ps: "\u06a9\u0648\u0631 \u067e\u0627\u06bc\u0647",
      translate: true
    },
    "News Categories": {
      en: "News Categories",
      dr: "\u062e\u0628\u0631\u0647\u0627",
      ps: "\u062e\u0628\u0631\u0648\u0646\u0647",
      translate: true
    },
    "Pajhwok Services": {
      en: "Pajhwok Services",
      dr: "\u062e\u062f\u0645\u0627\u062a \u067e\u0698\u0648\u0627\u06a9",
      ps:
        "\u062f \u067e\u0698\u0648\u0627\u06a9 \u062e\u062f\u0645\u062a\u0648\u0646\u0647",
      translate: true
    },
    "Video Service": {
      en: "Video Service",
      dr: "\u0628\u062e\u0634 \u0648\u064a\u062f\u064a\u0648\u064a\u0649",
      ps:
        "\u062f \u0648\u064a\u0689\u064a\u0648 \u0685\u0627\u0646\u06ab\u0647",
      translate: true
    },
    "Photo Service": {
      en: "Photo Service",
      dr: "\u0628\u062e\u0634 \u0639\u06a9\u0627\u0633\u0649",
      ps:
        "\u062f \u0627\u0646\u0681\u0648\u0631\u0648\u0646\u0648 \u0685\u0627\u0646\u06ab\u0647",
      translate: true
    },
    "Audio Service": {
      en: "Audio Service",
      dr: "\u0628\u062e\u0634 \u0635\u0648\u062a\u0649",
      ps: "\u063a\u0696\u064a\u0632\u0647 \u0685\u0627\u0646\u06ab\u0647",
      translate: true
    },
    "Application Settings": {
      en: "Application Settings",
      dr: "Application Settings",
      ps: "Application Settings",
      translate: false
    },
    Login: {
      en: "Login",
      dr: "\u0648\u0631\u0648\u062f",
      ps: "\u0646\u0646\u0648\u062a\u0644",
      translate: true
    },
    "Switch Language": {
      en: "Switch Language",
      dr: "\u062a\u063a\u064a\u064a\u0631 \u0632\u0628\u0627\u0646",
      ps: "\u062f \u0698\u0628\u06d0 \u0628\u062f\u0644\u0648\u0644",
      translate: true
    },
    "Font Size": {
      en: "Font Size",
      dr: "\u0627\u0646\u062f\u0627\u0632\u0647 \u062e\u0637",
      ps: "\u062f\u062e\u0637 \u0627\u0646\u062f\u0627\u0632\u0647",
      translate: true
    },
    Information: {
      en: "Information",
      dr: "\u0645\u0639\u0644\u0648\u0645\u0627\u062a",
      ps: "\u0645\u0639\u0644\u0648\u0645\u0627\u062a",
      translate: true
    },
    "Unread News": {
      en: "Unread News",
      dr:
        "\u0627\u062e\u0628\u0627\u0631 \u0646\u0627\u062e\u0648\u0627\u0646\u062f\u0647 \u0634\u062f\u0647",
      ps:
        "\u0646\u0627\u0644\u0648\u0633\u062a\u0644 \u0634\u0648\u064a \u062e\u0628\u0631\u0648\u0646\u0647",
      translate: true
    },
    "About Us": {
      en: "About Us",
      dr: "\u062f\u0631\u0628\u0627\u0631\u0647 \u0645\u0627",
      ps: "\u0632\u0645\u0648\u0696 \u067e\u0647 \u0627\u0693\u0647",
      translate: true
    },
    "Terms \u0026 Conditions": {
      en: "Terms \u0026 Conditions",
      dr: "Terms \u0026 Conditions",
      ps: "Terms \u0026 Conditions",
      translate: false
    },
    "Contact Us": {
      en: "Contact Us",
      dr: "\u062a\u0645\u0627\u0633 \u0628\u0627 \u0645\u0627",
      ps:
        "\u0644\u0647 \u0645\u0648\u0696 \u0633\u0631\u0647 \u0627\u0693\u06cc\u06a9\u0647",
      translate: true
    },
    "Personal Subscription": {
      en: "Personal Subscription",
      dr: "\u0627\u0634\u062a\u0631\u0627\u06a9 \u0634\u062e\u0635\u0649",
      ps: "\u0634\u062e\u0635\u064a \u06ab\u0689\u0648\u0646",
      translate: true
    },
    Feedback: {
      en: "Feedback",
      dr: "\u0646\u0638\u0631",
      ps: "\u0646\u0638\u0631",
      translate: true
    },
    "Data you are searching for is not available. Please change your search": {
      en:
        "Data you are searching for is not available. Please change your search",
      dr:
        "Data you are searching for is not available. Please change your search",
      ps:
        "Data you are searching for is not available. Please change your search",
      translate: false
    },
    "Forgot Password": {
      en: "Forgot Password",
      dr:
        "\u0631\u0645\u0632 \u0641\u0631\u0627\u0645\u0648\u0634 \u0634\u062f\u0647 ",
      ps:
        "\u0647\u06cc\u0631 \u0634\u0648\u06cc \u067e\u0633\u0648\u0631\u062f",
      translate: true
    },
    Email: {
      en: "Email",
      dr: "\u0627\u064a\u0645\u064a\u0644 \u0622\u062f\u0631\u0633",
      ps: "\u0628\u0631\u06d0\u069a\u0646\u0627\u0644\u064a\u06a9",
      translate: true
    },
    "Pajhwok Afghan News": {
      en: "Pajhwok Afghan News",
      dr:
        "\u067e\u0698\u0648\u0627\u06a9 \u062e\u0628\u0631\u064a \u0627\u0698\u0627\u0646\u0633",
      ps:
        "\u0622\u0698\u0627\u0646\u0633 \u062e\u0628\u0631\u06cc \u067e\u0698\u0648\u0627\u06a9",
      translate: true
    },
    SignIn: { en: "SignIn", dr: "SignIn", ps: "SignIn", translate: false },
    "Email Field is Empty": {
      en: "Email Field is Empty",
      dr: "Email Field is Empty",
      ps: "Email Field is Empty",
      translate: false
    },
    "Password Field is Empty": {
      en: "Password Field is Empty",
      dr: "Password Field is Empty",
      ps: "Password Field is Empty",
      translate: false
    },
    "Password is Too Short": {
      en: "Password is Too Short",
      dr: "Password is Too Short",
      ps: "Password is Too Short",
      translate: false
    },
    "Email Pattern is Wrong": {
      en: "Email Pattern is Wrong",
      dr: "Email Pattern is Wrong",
      ps: "Email Pattern is Wrong",
      translate: false
    },
    Password: {
      en: "Password",
      dr: "\u0631\u0645\u0632",
      ps: "\u067e\u0633\u0648\u0631\u062f",
      translate: true
    },
    SignUp: { en: "SignUp", dr: "SignUp", ps: "SignUp", translate: false },
    "Username is Empty": {
      en: "Username is Empty",
      dr: "Username is Empty",
      ps: "Username is Empty",
      translate: false
    },
    Name: {
      en: "Name",
      dr: "\u0627\u0633\u0645",
      ps: "\u0646\u0648\u0645",
      translate: true
    }
  }
};

export default English;
