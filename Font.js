export let allFont = {
  fontSizeNormal: 16,
  fontSizeTitle: 26,
  fontSizeSmall: 12,
  fontSizeSubTitle: 20
};
export var fontSizeNormal = allFont.fontSizeNormal;
export var fontSizeTitle = allFont.fontSizeTitle;
export var fontSizeSmall = allFont.fontSizeSmall;
export var fontSizeSubTitle = allFont.fontSizeSubTitle;
