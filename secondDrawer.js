import React, { Component } from "react";
import { DrawerNavigator } from "react-navigation";
import Home2 from "./home2";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import AboutUs from "./component/AboutUs/index";
import TermsAndConditions from "./component/TermsandCondition/index";
import ContactUs from "./component/ContactUs/index";
import singleNews from "./component/singleNews/index";
import Category from "./component/Category/index";
import Search from "./component/Search/index";
import Feedback from "./component/Feedback/index";
import SingleVideo from "./component/singleVideo";
import SingleImage from "./component/SingleImage";
import groupVideo from "./component/groupVideo";
import groupImage from "./component/groupImage";
import Drawer2Content from "./component/Drawer2Content";
import FontSize from "./component/FontSize";
import GroupInsideImage from "./component/groupInsideImage";

class SecondDrawer extends Component {
  componentDidMount = async () => {
    await this.props.change({ name: "drawer", value: this.props.navigation });
  };
  render() {
    const A = DrawerTwo();
    return <A />;
  }
}

const DrawerTwo = () =>
  DrawerNavigator(
    {
      Homeee: Home2,
      AboutUs: AboutUs,
      TermsAndConditions: TermsAndConditions,
      ContactUs: ContactUs,
      SingleNews: singleNews,
      Category: Category,
      Search: Search,
      Feedback: Feedback,
      SingleVideo: SingleVideo,
      SingleImage: SingleImage,
      GroupVideo: groupVideo,
      GroupImage: groupImage,
      FontSize: FontSize,
      GroupInsideImage: GroupInsideImage
    },
    {
      initialRouteName: "Homeee",
      drawerPosition: "right",
      drawerWidth: 300,
      contentComponent: Drawer2Content
    }
  );

const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    lan: state.reducer.lan,
    drawer: state.reducer.drawer
  };
};
const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SecondDrawer)
);
