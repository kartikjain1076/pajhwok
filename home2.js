import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Dimensions,
  RefreshControl,
  NetInfo
} from "react-native";
import SplashScreen from "react-native-splash-screen";
import Header from "./component/header";
import axios from "axios";
import Slideshow from "react-native-slideshow";
import Recent from "./component/Recent/index";
import Features from "./component/Features/index";
import Interviews from "./component/Interviews/index";
import Video from "./component/Video/index";
import Photos from "./component/Photos/index";
import Opinion from "./component/Opinion/index";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";
import * as Progress from "react-native-progress";
import { baseURL } from "./config";
import SearchBar from "./component/SearchBar";
import lanArr from "./lanArr";

class Home2 extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      position: 1,
      interval: null,
      refreshing: false,
      isOnline: true
    };
  }
  // function for pull to refresh
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.fetchImageSlider().then(() => {
      this.setState({ refreshing: false });
    });
  };

  componentDidMount = async () => {
    this.props.drawer.closeDrawer();
    SplashScreen.hide();
    await this.props.change({
      name: "childDrawer",
      value: this.props.navigation
    });
    await this.fetchImageSlider();
    await this.fetchHardCodeContent();
  };
  // fetch the language translation data
  fetchHardCodeContent = async () => {
    const response = await axios.get(
      `${baseURL}json/string_translate?string=${lanArr}`
    );
    this.props.change({ name: lan, value: response.data });
  };

  // fetch the data for component on the home screen
  fetchImageSlider = async () => {
    let response = await axios.get(
      `${baseURL}${this.props.language}/json/home_slider`
    );
    let data = {
      name: "slideshow",
      value: [
        {
          title: response.data[0].title,
          url: response.data[0].image,
          nid: response.data[0].nid
        },
        {
          title: response.data[1].title,
          url: response.data[1].image,
          nid: response.data[1].nid
        },
        {
          title: response.data[2].title,
          url: response.data[2].image,
          nid: response.data[2].nid
        },
        {
          title: response.data[3].title,
          url: response.data[3].image,
          nid: response.data[3].nid
        },
        {
          title: response.data[4].title,
          url: response.data[4].image,
          nid: response.data[4].nid
        }
      ]
    };
    this.props.change(data);
    response = await axios.get(
      `${baseURL}${this.props.language}/json/recent_article?front=1&limit=5`
    );
    data = { name: "recent", value: response.data };
    this.props.change(data);
    response = await axios.get(
      `${baseURL}${
        this.props.language
      }/json/category_content?tid=27&front=1&limit=3`
    );
    data = { name: "feature", value: response.data };
    this.props.change(data);
    response = await axios.get(
      `${baseURL}${
        this.props.language
      }/json/category_content?tid=28&front=1&limit=3`
    );
    data = { name: "interview", value: response.data };
    this.props.change(data);
    response = await axios.get(
      `${baseURL}${this.props.language}/json/new_video_content?front=1&limit=4`
    );
    data = { name: "video", value: response.data };
    this.props.change(data);
    response = await axios.get(
      `${baseURL}${this.props.language}/json/photo_content?front=1&limit=4`
    );
    data = { name: "photo", value: response.data };
    this.props.change(data);
    response = await axios.get(
      `${baseURL}${this.props.language}/json/recent_opinion?front=1&limit=3`
    );
    data = { name: "opinion", value: response.data };
    this.props.change(data);
  };

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.props.slideshow.length
              ? 0
              : this.state.position + 1
        });
      }, 2000)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  // return the progress spinner while data is being fetched and after that return the data.
  showLoading = () => {
    if (
      this.props.feature.length == 0 &&
      this.props.interview.length == 0 &&
      this.props.photo.length == 0 &&
      this.props.video.length == 0 &&
      this.props.opinion.length == 0
    ) {
      return (
        <View style={styles.progress}>
          <Progress.Circle size={30} indeterminate={true} color="#253C80" />
        </View>
      );
    } else {
      return (
        <View>
          <Recent />
          <Features />
          <Interviews />
          <Video />
          <Photos />
          <Opinion />
        </View>
      );
    }
  };

  render() {
    return (
      <ScrollView
        stickyHeaderIndices={[0]}
        keyboardShouldPersistTaps="always"
        style={styles.main}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
      >
        <View>
          <Header />
        </View>
        <View>
          <SearchBar />
        </View>
        <Slideshow
          dataSource={this.props.slideshow}
          position={this.state.position}
          onPositionChanged={position => this.setState({ position })}
          onPress={() =>
            this.props.navigation.navigate("SingleNews", {
              nid: this.props.slideshow[this.state.position].nid
            })
          }
        />
        {this.showLoading()}
      </ScrollView>
    );
  }
}

const { height, width } = Dimensions.get("window");

const styles = StyleSheet.create({
  searchView: {
    height: 40,
    marginLeft: "2%",
    marginRight: "2%",
    marginTop: "2%",
    flexDirection: "row"
  },
  main: {
    height: "100%",
    width: "100%",
    backgroundColor: "#ffffff"
  },
  progress: {
    marginLeft: (width / 100) * 45,
    marginTop: (height / 100) * 1
  }
});

const mapStateToProps = state => {
  return {
    language: state.reducer.language,
    slideshow: state.reducer.slideshow,
    recent: state.reducer.recent,
    feature: state.reducer.feature,
    interview: state.reducer.interview,
    video: state.reducer.video,
    opinion: state.reducer.opinion,
    photo: state.reducer.photo,
    childDrawer: state.reducer.childDrawer,
    drawer: state.reducer.drawer,
    lan: state.reducer.lan
  };
};

const mapDispatchToProps = dispatch => {
  return {
    change: data => dispatch({ type: data.name, value: data.value })
  };
};

export default withNavigation(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home2)
);
